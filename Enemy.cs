﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Serialization_Deserialization_
{
    [Serializable]
    public class Enemies
    {
        protected string name;
        protected int health;

        public Enemies (string _name, int _health)
        {
            name = _name;
            health = _health;
        }

        public virtual void arrive()
        {

        }
    }
}

