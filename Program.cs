using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Serialization_Deserialization_
{
    class Program
    {
        static void Main(string[] args)
        {
            //Raw Data
            Dragon dragon = new Dragon("Smaug", 1000);
            Goblin goblin = new Goblin("Gollum", 100);



            //AfterSerialization

            Enemies[] enemies = { dragon, goblin };
            string filePath = "data.save";
            DataSerializer dataSerializer = new DataSerializer();
            Enemies e = null;

            dataSerializer.BinarySerialize(enemies, filePath);

            e = dataSerializer.BinaryDeserialize(filePath) as Enemies;

            //Console.WriteLine(e.Goblin);
            //Console.WriteLine(e.Dragon);

            ObjectOutput(dragon);
            ObjectOutput(goblin);

        }


        static void ObjectOutput(Enemies _enemies)
        {
            _enemies.arrive();
        }

    }

    //[Serializable]
    //public class Enemy
    //{
    //    public string Goblin { get; set; }
    //    public string Dragon { get; set; }
    //}

        // Serialization 
    class DataSerializer
    {
        public void BinarySerialize(object data, string filePath)
        {
            FileStream fileStream;
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(filePath)) File.Delete(filePath);
            fileStream = File.Create(filePath);

            bf.Serialize(fileStream, data);
            fileStream.Close();
        }


        public object BinaryDeserialize(string filepath)
        {
            object obj = null;

            FileStream filestream;
            BinaryFormatter bf = new BinaryFormatter();

            if (File.Exists(filepath))
             {
                filestream = File.OpenRead(filepath);
                obj = bf.Deserialize(filestream);
                filestream.Close();
              }

        return obj;

        }
    }

}
