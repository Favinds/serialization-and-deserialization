﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialization_Deserialization_
{
    [Serializable]
    public class Goblin : Enemies
    {
        public Goblin(string _name, int _health) : base(_name, _health)
        {

        }

        public override void arrive()
        {
            Console.WriteLine("The " + name + " is arive! " + " It have " + health + " health" );
        }
    }
}
